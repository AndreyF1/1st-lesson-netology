﻿<?php
$title = 'Andrey Katkov about';
$h1 = 'Страница Андрея Каткова';
$name = "Андрей";
$age = 26;
$email = "andreykatkov13@gmail.com";
$city = "Москва";
$about = "CRM-менеджер";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><?= $title ?></title
</head>
<body>
	<h1><?= $h1 ?></h1>
	<table  style="text-align: left; border-spacing: 50px 10px;">
		<tr>
			<td>Имя</td>
			<td><?= $name ?></td>
		</tr>
		<tr>
			<td>Возраст</td>
			<td><?= $age ?></td>
		</tr>
		<tr>
			<td>E-mail</td>
			<td><a href="mailto:<?= $email ?>" target="_blank"><?= $email ?></a></td>
		</tr>
		<tr>
			<td>Город</td>
			<td><?= $city ?></td>
		</tr>
		<tr>
			<td>О себе</td>
			<td><?= $about ?></td>
		</tr>
	</table>
</body>
</html>

